var conn = require('../models/main')
var fs = require("fs")
var sql = require("mssql")

module.exports.getDataTable = function (d, cred) {
  return new Promise(function (resolve, reject) {
    sql.close();
    var config = JSON.parse(fs.readFileSync(__dirname + '/cred', 'utf8'));
    config.user = cred.user
    config.password = cred.pass
    sql.connect(config, function (err) {
      if (err) {
        resolve({ err: true, description: err })
      } else {
        var request = new sql.Request();
        request.query(`

        SELECT
          *
        FROM
          ${d.tabla}

        `, function (err, recordset) {
            if (err) console.log(err)
            resolve({ err: false, data: recordset })
          });
      }
    });
  })
}
