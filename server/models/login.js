var sql = require("mssql");
var fs = require('fs')

module.exports.doLogin = function (d) {
  return new Promise(function (resolve, reject) {
    sql.close();
    var config = JSON.parse(fs.readFileSync(__dirname + '/cred', 'utf8'));
    config.user = d.user
    config.password = d.pass

    sql.connect(config, function (err) {
      if (err) {
        resolve({ err: true, description: err })
      } else {
        var request = new sql.Request();
        request.query('select 1+1 as res', function (err, recordset) {
          if (err) console.log(err)
          resolve({ err: false, data: recordset })
        });
      }
    });
  })
}

module.exports.getData = function (d) {
  return new Promise(function (resolve, reject) {
    sql.close();
    var config = JSON.parse(fs.readFileSync(__dirname + '/cred', 'utf8'));
    config.user = d.user
    config.password = d.pass

    sql.connect(config, function (err) {
      if (err) {
        resolve({ err: true, description: err })
      } else {
        var request = new sql.Request();
        request.query(`
        
        SELECT
          COUNT(a2.asunto) AS total,
          a2.asunto,
          a2.fecha,
          a2.cafeteria,
          a2.proyector,
          a2.pizarron
        FROM  
          test.dbo.agenda_participante AS a1
        INNER JOIN  
          test.dbo.agenda AS a2
        ON
          a1.agenda_id = a2.agenda_id
        INNER JOIN  
          test.dbo.participante AS a3
        ON
          a1.participante_id = a3.participante_id
        GROUP BY 
          a2.asunto, a2.fecha, a2.cafeteria, a2.proyector, a2.pizarron, a2.activo
        HAVING 
          a2.activo = 1
        
        `, function (err, recordset) {
            if (err) console.log(err)
            resolve({ err: false, data: recordset })
          });
      }
    });
  })
}