var Home = require('../models/login')

module.exports.doLogin = function (d) {
  return new Promise(async function (resolve, reject) {
    if (d.user === undefined || d.pass === undefined) {
      resolve({ err: true, description: 'Campos invalidos' })
    } else {
      let aw = await Home.doLogin(d)
      resolve(aw)
    }
  })
}

module.exports.getData = function (d) {
  return new Promise(async function (resolve, reject) {
    let aw = await Home.getData(d)
    resolve(aw)
  })
}
