var express = require('express');
var router = express.Router();


router.get('/', function(req, res) {
  if (req.session.usuario) {
    res.redirect('inicio')
  } else {
    res.redirect('login')
  }
});

router.get('/inicio', function(req, res) {
  if (req.session.usuario) {
    res.render('inicio', {usuario:true, url : 'http://192.168.1.74:3003/'})
  } else {
    res.redirect('login')
  }
});

router.get('/tabla/:tabla_id', function(req, res) {
  if (req.session.usuario) {
    var tabla_id = req.params.tabla_id
    res.render('tabla', {usuario:true, url : 'http://192.168.1.74:3003/', tabla_id: tabla_id})
  } else {
    res.redirect('/login')
  }
});

router.get('/login', function(req, res) {
  if (req.session.usuario) {
    res.redirect('/')
  } else {
    res.render('login', {usuario:false, url : 'http://192.168.1.74:3003/'})
  }
});

module.exports = router;
