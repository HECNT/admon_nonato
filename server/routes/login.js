var express = require('express');
var router = express.Router();
var Home = require('../controllers/login');
// METHOD'S
// GET
router.get('/kill', doKill)
router.get('/get-data', getData)

router.post('/', doLogin)

async function doLogin(req, res) {
  var d = req.body;
  let aw = await Home.doLogin(d)
  if (aw.err) {
    res.json(aw)
  } else {
    req.session.usuario = d
    res.json(aw) 
  }
}

async function doKill(req, res) {
  req.session.usuario = null
  res.json({err: false})
}

async function getData(req, res) {
  var d = req.session.usuario
  let aw = await Home.getData(d)
  res.json(aw)
}

module.exports = router;
