var express = require('express');
var router = express.Router();
var Home = require('../controllers/home');

router.post('/get-data-table', getDataTable)

function getDataTable(req, res) {
  var d = req.body;
  var cred = req.session.usuario;
  Home.getDataTable(d, cred)
  .then(function(result){
    res.json(result);
  })
}


module.exports = router;
