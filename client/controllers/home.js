require('../services/home');


angular.module(MODULE_NAME)
.controller('homeCtrl', ['$scope', 'HomeService', 'LoginService', '$timeout', function($scope, HomeService, LoginService, $timeout) {
  var ctrl = this;
  $scope.hola = "new life"

  $scope.btnCierra = btnCierra;
  $scope.init = init;
  $scope.initTabla = initTabla;

  function btnCierra() {
    HomeService.deleteSession()
    .then(function(res){
      location.reload()
    })
  }

  function initTabla(id) {
    var d = {tabla: ""}
    if (id === 1) {
      $scope.msg_tabla = "Tabla Usuario"
      d.tabla = "usuario"
      $scope.usuarioS = true
    }
    if (id === 2) {
      $scope.msg_tabla = "Tabla Perfil"
      d.tabla = "perfil"
      $scope.perfilS = true
    }
    if (id === 3) {
      $scope.msg_tabla = "Tabla Sala"
      d.tabla = "sala"
      $scope.salaS = true
    }
    if (id === 4) {
      $scope.msg_tabla = "Tabla Agenda"
      d.tabla = "agenda"
      $scope.agendaS = true
    }
    if (id === 5) {
      $scope.msg_tabla = "Tabla Agenda Participante"
      d.tabla = "agenda_participante"
      $scope.agenda_participanteS = true
    }
    HomeService.getDataTable(d)
    .success(function(res){
      $scope.list = res.data.recordset;
    })
  }

  function init() {
    console.log('dsadsadsad');

    LoginService.getData()
    .then(function(res){
      if (res.err) {
        alert('Hubo un error' + res.description)
      } else {
        $scope.list = res.data.data.recordset;
      }
    })
  }

}]);

    angular.module(MODULE_NAME)
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
