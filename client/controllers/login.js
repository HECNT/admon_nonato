require('../services/home');
require('../services/login');
var md5 = require('md5');

angular.module(MODULE_NAME)
  .controller('loginCtrl', ['$scope', 'LoginService', '$timeout', function ($scope, LoginService, $timeout) {
    var ctrl = this;
    $scope.login = {}
    $scope.msg_err = false
    $scope.msg_err_t = ""
    //Function
    $scope.btnDoLogin = btnDoLogin;

    function btnDoLogin() {
      var d = $scope.login
      if (d.user === undefined || d.pass === undefined) {
        $scope.msg_err = true
        $scope.msg_err_t = "Los campos usuario y contraseña son requeridos"
      } else {
        $scope.msg_err = false
        LoginService.doLogin(d)
          .then(function (res) {
            if (res.data.err) {
              $scope.msg_err = true
              $scope.msg_err_t = res.data.description.originalError.message
            } else {
              $scope.msg_err = false
              $scope.msg_err_t = ""
              location.reload()
            }
          })
      }
    }

  }]);

angular.module(MODULE_NAME)
  .directive('fileModel', ['$parse', function ($parse) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;
        element.bind('change', function () {
          scope.$apply(function () {
            modelSetter(scope, element[0].files[0]);
          });
        });
      }
    };
  }]);
