// var url = helpers.getUrl();

angular.module(MODULE_NAME)
.service('HomeService', ['$http', function($http) {
  var url = "http://192.168.1.74:3003";
  var urlBase = url + '/home';
  var urlBaseL = url + '/login';

  this.deleteSession = function(d) {
    return $http.get(urlBaseL + '/kill')
  }

  this.getDataTable = function(d) {
    return $http.post(urlBase + '/get-data-table', d)
  }

}]);
