// var url = helpers.getUrl();

angular.module(MODULE_NAME)
.service('LoginService', ['$http', function($http) {
  var url = "http://192.168.1.74:3003";
  var urlBase = url + '/login';

  this.doLogin = function(d) {
    return $http.post(urlBase, d)
  }

  this.getData = function() {
    return $http.get(urlBase + '/get-data')
  }

}]);
